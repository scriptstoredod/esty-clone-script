Our Etsy Clone is an easy way to earn money. You can sell your own items, products or supplies made by others, or simply charge a fee for subscribers who sell through your marketplace. Just like the original it supports unlimited products, unlimited number of users who can both buy and sell, and you can manage all that from a single administrator panel.

User Demo

Admin Demo


Document


PRODUCT DESCRIPTION
General Features:
Frontend upload products
Support 200+ payment gateways
Support physical & digital products
Multi vendor supported
Responsive Design
Commission & Withdrawal method available
Multi currency
Multi language
Social login (fb,twitter,google + etc)
Newsletter
SEO friendly
Unique Features:
Etsy clone supports attractive admin dashboard for easy management of CMS, discount store, products and category management
Extensive search facility through keywords, category, deals and discounts, etc.
Advanced Filter and Sort Module over search pages for precise products
Supports easy creation of store for the seller for selling their products
Standard Payment Gateway System integrated for secured payment transactions
Discount module lets you manage store image, description, guidelines and history – thus giving you more control over the way you would want to run your discount store
Etsy clone is SEO friendly which will help you from marketing point of view to market your website at nominal prices. Not only that, other features such as the promotional panel on the homepage, social sharing widgets, star ratings, testimonials and newsletter management modules in the Admin Panel will give more power to your marketing efforts
Buyers can join communities created by sellers to participate in a discussion forum and thread
Create Shops/Stores
Sellers can create their own shops to sell their items on.
Multiple Payment Options
Sellers can choose to accept payments using Paypal, Payza, Cheque, Money Order or Other
RESPONSIVE DESIGN – MOBILE FRIENDLY
Etsy Clone Script is fully responsive so it works on mobile devices!
CURRENCY SYSTEM
There is one main currency, and then other currencies can be added with an exchange rate. Users can change the currency to have prices shown in their preferred currency.
LANGUAGE SYSTEM
Etsy Clone Script supports multiple languages for users to choose from. You can easily add more.
REGION SYSTEM
Users can choose a region, then rearch results will only show items that ship to that region.


Check out website:
https://www.doditsolutions.com/
http://scriptstore.in/
http://phpreadymadescripts.com/